package puskomcore

// HTTPResponse struct
type HTTPResponse struct {
	Code    int                    `json:"code"`
	Message string                 `json:"message,omitempty"`
	Data    map[string]interface{} `json:"data,omitempty"`
	Errors  []string               `json:"errors,omitempty"`
}
