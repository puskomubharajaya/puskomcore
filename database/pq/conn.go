package pq

import (
	"fmt"
	"log"
	"os"

	"bitbucket.org/puskomubharajaya/puskomcore/database"
	_ "github.com/lib/pq"
)

func init() {

	urlConnection := "user=" + fmt.Sprint(os.Getenv("db_user")) + " "
	if os.Getenv("db_password") != "" {
		urlConnection += "password=" + fmt.Sprint(os.Getenv("db_password")) + " "
	}
	urlConnection += "host=" + fmt.Sprint(os.Getenv("db_host")) + " "
	urlConnection += "port=" + fmt.Sprint(os.Getenv("db_port")) + " "
	urlConnection += "dbname=" + fmt.Sprint(os.Getenv("db_database")) + " "
	urlConnection += "sslmode=disable"

	log.Println("Connecting to DB Server " + fmt.Sprint(os.Getenv("db_host")) + ":" + fmt.Sprint(os.Getenv("db_port")) + "...")

	database.Register("postgres", urlConnection)
}
