package mysql

import (
	"fmt"
	"log"
	"os"

	"bitbucket.org/puskomubharajaya/puskomcore/database"
	// driver dialect for apm
	_ "go.elastic.co/apm/module/apmsql/mysql"
)

func init() {
	usernameAndPassword := fmt.Sprint(os.Getenv("db_user")) + ":" + fmt.Sprint(os.Getenv("db_password"))
	hostName := "tcp(" + fmt.Sprint(os.Getenv("db_host")) + ":" + fmt.Sprint(os.Getenv("db_port")) + ")"

	log.Println("Connecting to DB Server " + fmt.Sprint(os.Getenv("db_host")) + ":" + fmt.Sprint(os.Getenv("db_port")) + "...")
	urlConnection := usernameAndPassword + "@" + hostName + "/" + fmt.Sprint(os.Getenv("db_database")) + "?charset=utf8&parseTime=true&loc=UTC"

	database.Register("mysql", urlConnection)
}
