package puskomcore

// CtxKey hold context key for context.WithValue
type CtxKey struct {
	Key         int
	Description string
}

var (
	// AuthInfo hold key for auth info
	AuthInfo = &CtxKey{Key: 1, Description: "Auth Info"}
)
