package runtime

import (
	"bitbucket.org/puskomubharajaya/puskomcore"
	"github.com/labstack/echo/v4"
	"gopkg.in/go-playground/validator.v9"

	"github.com/go-playground/locales/id"
	ut "github.com/go-playground/universal-translator"
	idTrans "gopkg.in/go-playground/validator.v9/translations/id"
)

var translator ut.Translator

func setValidator(e *echo.Echo) {
	id := id.New()
	uni := ut.New(id, id)

	translator, _ = uni.GetTranslator("id")
	validator := validator.New()
	_ = idTrans.RegisterDefaultTranslations(validator, translator)
	e.Validator = &puskomcore.CustomValidator{Validator: validator}
}
