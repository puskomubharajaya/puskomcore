package runtime

import (
	"bitbucket.org/puskomubharajaya/puskomcore/grpc/codes"
	"bitbucket.org/puskomubharajaya/puskomcore/http"
	"github.com/labstack/echo/v4"
	"google.golang.org/genproto/googleapis/rpc/errdetails"
	grpcCodes "google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"gopkg.in/go-playground/validator.v9"
)

// CustomErrorHandler for handling Echo Recovery
func CustomErrorHandler(c *echo.Echo) {
	setValidator(c)
	c.HTTPErrorHandler = func(err error, c echo.Context) {

		// Validation Error
		if errs, ok := err.(validator.ValidationErrors); ok {
			var message []string

			translated := errs.Translate(translator)
			for _, v := range translated {
				message = append(message, v)
			}
			resp := http.Response{
				Code:    codes.InvalidArgument,
				Message: codes.StatusMessage[codes.InvalidArgument],
				Errors:  message,
			}
			_ = resp.JSON(c)
			return
		}

		// GRPC Error
		if st, ok := status.FromError(err); ok {
			resp := http.Response{
				Code:    st.Code(),
				Message: codes.StatusMessage[st.Code()],
				Errors:  []string{st.Message()},
			}

			// Handle Cancelled context
			if st.Code() == grpcCodes.Canceled {
				resp = http.Response{
					Code:    codes.RequestTimeout,
					Message: codes.StatusMessage[codes.RequestTimeout],
					Errors:  []string{codes.StatusMessage[codes.RequestTimeout]},
				}
			}

			var errDetails []string
			if len(st.Details()) > 0 {
				for _, detail := range st.Details() {
					switch t := detail.(type) {
					case *errdetails.BadRequest:
						for _, violation := range t.GetFieldViolations() {
							errDetails = append(errDetails, violation.GetDescription())
						}
					}
				}

				resp.Errors = errDetails
			}
			_ = resp.JSON(c)
			return
		}
		resp := http.Response{
			Code:    codes.InternalError,
			Message: codes.StatusMessage[codes.InternalError],
			Errors:  []string{err.Error()},
		}
		_ = resp.JSON(c)
		return
	}

}
