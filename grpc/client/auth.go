package client

import (
	"context"
	"log"
	"os"

	pb "bitbucket.org/puskomubharajaya/puskomcore/proto/account/auth"
	"go.elastic.co/apm/module/apmgrpc"
	"google.golang.org/grpc"
)

// AuthClientToken grpc client
func AuthClientToken(ctx context.Context, r *pb.AuthClientTokenRequest) (
	*pb.AuthClientTokenResponse, error) {
	addr := os.Getenv("AUTH_SERVICE_ADDRESS")
	if addr == "" {
		addr = "1001"
	}

	conn, err := grpc.Dial(addr, grpc.WithInsecure(),
		grpc.WithUnaryInterceptor(apmgrpc.NewUnaryClientInterceptor()))
	if err != nil {
		log.Fatal("could not connect to", addr, err)
	}

	defer conn.Close()
	client := pb.NewAuthClient(conn)

	return client.AuthClientToken(ctx, r)
}
