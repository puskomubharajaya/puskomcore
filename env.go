package puskomcore

import (
	"fmt"
	"os"
	"strings"
)

// Environment struct
type Environment struct {
	config map[string]interface{}
}

// Set environment variable
func (e *Environment) Set(key, value string) {
	e.config[key] = value
	os.Setenv(key, value)
}

// Get environment variable
func (e *Environment) Get(key string) string {
	return os.Getenv(key)
}

// Load environment from file
func (e *Environment) Load() {
	e.loadFile(false)
}

// Override load environment and replace existing
func (e *Environment) Override() {
	e.loadFile(true)
}

// GetConfig return desired config
func (e *Environment) GetConfig() map[string]interface{} {
	return e.config
}

func (e *Environment) loadFile(overload bool) {
	currentEnv := map[string]bool{}
	rawEnv := os.Environ()
	for _, rawEnvLine := range rawEnv {
		key := strings.Split(rawEnvLine, "=")[0]
		currentEnv[key] = true
	}

	for key, value := range e.config {
		if !currentEnv[key] || overload {
			val := fmt.Sprintf("%v", value)
			os.Setenv(key, val)
		}
	}
}

// Env create new Environment
func Env(path string) Environment {
	var environment Environment
	environment = Environment{Config(path)}
	environment.Load()

	return environment
}
